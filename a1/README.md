> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Katherine Weachter

### Assignment 1 # Requirements:

*Three parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chp 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello;
* Screenshot of running http://localhost:9999;
* git commands with short descriptions;
* Bitbucket repo links a)this Assignment and b) the completed tutorial above


> #### Git commands w/short descriptions:

1. git init - creates a new git repository 
2. git status - used to see what has been modified 
3. git add - used to start tracking new files
4. git commit - records changes to the local repository
5. git push - transfers the last commit to a remote repo
6. git pull - Used to update current repo with the latest changes from the remote repository
7. git config - Sets repository or global options

#### Assignment 1 Screenshots:

*Screenshot of Tomcat Installation:

![Tomcat Installation Screenshot](/a1/img/tomcat_install.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](/a1/img/JDK_install.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kmw14f/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/kmw1f/myteamquotes/ "My Team Quotes Tutorial")

