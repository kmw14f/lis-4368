# LIS 4368

## Katherine Weachter

### Assignment 3 # Requirements:

1. Download and install MySql Workbench 
2. Create ERD using business rules 
3. Forward-engineer ERD to cci-student server
4. Write SQL Statements 


#### README.md file should include the following items:

* [a3.sql](docs/a3.sql)
* [a3.mwb](docs/a3.mwb)
	

#### Assignment 3 Screenshot:

*Screenshot of ERD*

![ERD Screenshot](/a3/img/a3.png)
