> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Katherine Weachter

### Assignment 2 # Requirements:

*Four parts:*

1. Download and install mysql
2. Create new user and password
3. Develop and deploy a WebApp
4. Write and invoke Java servlets

#### README.md file should include the following items:

* http://localhost:9999/hello(displaysdirectory,needsindex.html)
* http://localhost:9999/hello/HelloHome.html
* http://localhost:9999/hello/sayhello (invokes HelloServlet)
* http://localhost:9999/hello/querybook.html
* http://localhost:9999/hello/sayhi(invokesAnotherHelloServlet)


#### Assignment 2 Screenshots:

*Screenshot of Query Results*:

![Query Results Screenshot](/a2/img/a2.png)

