import java.util.Scanner;

public class GradeApp
{
   public static void main(String[] args)
   {
   
   
	  double value = 0;
	  double total = 0;
	  int count = 0;
      Scanner sc = new Scanner(System.in);
      
        
      System.out.println("Please enter grades that range from 0 to 100. \nGrade average and total is rounded to 2 decimal places.\n Note: Program does NOT check for non-numeric characters. \nTo end program enter -1.");
 
      
      do{
      
	System.out.print("enter grade:  ");
	value = sc.nextDouble();
	
	if (value > 100 || value < -1) { 
	System.out.println("Invalid entry, not counted.");
	count --;
	total -= value;
	
	}
	
	count++;
	total += value;
	
	}while (value != -1);
	
	int new_count = count -1;
	System.out.print("\nGrade count: " + new_count);
	
	double new_total = total +1; 
	System.out.printf("\nGrade total: %.2f", new_total);
	
	double average = new_total/new_count;
	System.out.printf("\nAverage grade: %.2f", average);
	System.out.println ("");
	
	}
	}