> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Katherine Weachter

### Assignment 5 # Requirements:


*4 Parts*

1. MVC framework
2. Implement CRUD
3. Server-side Validation
4. Regular expressions



#### Assignment 4 Screenshots:

*Screenshot of pre-validation*:

![Query Results Screenshot](/a5/img/prevalidation.png)

*Screenshot of results*

![Query Results Screenshot](/a5/img/results.png)


*Screenshot of MySQL table entry*:

![Query Results Screenshot](/a5/img/mysqlscreenshot.png)

