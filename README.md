> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368 and Advanced Web App

## Katie Weachter

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Setup Git and Bitbucket 
    - Install JDK
    - Install APache Tomcat
    - Git commands w/ short descriptions
    - Completed tutorial repos 

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Download and install mysql 
	- Create new user and password
	- Develop and deploy a WebApp
	- Write and invoke Java servlets
	
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Download and install MySql Workbench 
	- Create ERD using business rules 
	- Forward-engineer ERD to cci-student server
	- Write SQL Statements 
	
	
4. [P1 README.md](p1/README.md "My P1 README.md file")
	- Create online portfolio
	- Edit JSP files 
	- Create and modify regular expressions
	- Create and modify JavaScript 
	
5. [A4 README.md](a4/README.md "My A4 README.md file") 
	- Perform client validation 
	- Analyze and debug failed client validation
	- Utilize JSP forms
	- Edit JSP files
	
6. [A5 README.md](a5/README.md "My A5 README.md file") 
	- MVC framework
	- Implement CRUD
	- Server-side Validation
	- Regular expressions
	
7. [P2 README.md](p2/README.md "My P2 README.md file") 
	- MVC framework
	- Implement CRUD
	- Server-side Validation
	- Regular expressions






