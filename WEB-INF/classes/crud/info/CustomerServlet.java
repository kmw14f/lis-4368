package crud.info;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
 
import crud.business.Customer;
import crud.data.CustomerDB;

//servlet Customer is mapped to the URL pattern /customerList. When accessing this servlet, it will return a message.
@WebServlet("/customerAdmin")
public class CustomerServlet extends HttpServlet
{
	//perform different request data processing depending on transfer method (here, Post or Get)
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
	{
	
		HttpSession session = request.getSession();
		
		String url = "/index.jsp"; //initialize url value (used for logic below)
        
		// get current action
		String action = null;
		String cid = null;
		
		if (request.getParameter("display_customer") != null)
			{
			 action = "display_customer";
			 cid = request.getParameter(action);
			 }
			 
		else if (request.getParameter("add_customer") != null)
			{ 
		    action = "add_customer";
		    cid = null;
		    }
		    
		else if (request.getParameter("update_customer") != null)
		    {
		    action = "update_customer";
		    cid =  request.getParameter(action);
		    }
		
		else if (request.getParameter("delete_customer") != null)
			{
			 action = "delete_customer";
			 cid =  request.getParameter(action);
			 }
		
		else if (request.getParameter("thanks") != null)
			{ 
			  action = "join";
			}
			
		else
			{
			  url = "/index.jsp"; //main page
			}
		
		if (action.equals("join"))
		 {
		  url = "/customerform.jsp"; 
		 }
		 
		else if (action.equals("display_customer"))
		 {
		 	//String cid = request.getParameter("id");
		   Customer user = CustomerDB.selectCustomer(cid);
		   session.setAttribute("user", user);
		   url = "/customer.jsp";
		 }
		 
		else if (action.equals("add_customer"))
		 {
		 	String id = request.getParameter(null);
		 	String firstName = request.getParameter("fname");
		 	String lastName = request.getParameter("lname");
		 	String street = request.getParameter("street");
		 	String city = request.getParameter("city");
		 	String state = request.getParameter("state");
		 	String zip = request.getParameter("zip");
		 	String phone = request.getParameter("phone");
		 	String email = request.getParameter("email");
		 	String balance = request.getParameter("balance");
		 	String totalSales = request.getParameter("total_sales");
		 	String notes = request.getParameter("notes");
		 	
		 Customer user = new Customer(id, firstName, lastName, street, city, state, zip, phone, email, balance, totalSales, notes);
		 
		 //declare message variable
		 String message;
		 
		 if( firstName == null || lastName == null || street == null || 
		 city == null || state == null || zip == null || phone == null || 
		 email == null || balance == null || totalSales == null ||

		  	firstName.isEmpty() || lastName.isEmpty() ||
		  	street.isEmpty() ||  city.isEmpty() || state.isEmpty() ||
		  	zip.isEmpty() || phone.isEmpty() || email.isEmpty() ||
		  	balance.isEmpty() || totalSales.isEmpty()
		  	)
		  	
		  	
		  {
		  	message = "All text boxes required except Notes.";
		  	url = "/customerform.jsp";
		  }
		  
		else 
		  {
		  	message = "";
		  	url = "/thanks.jsp";
		  	CustomerDB.insert(user);
		  }
		request.setAttribute("user", user);
		request.setAttribute("message", message);
		  }
		  
		else if (action.equals("update_customer"))
		 {
		 	String firstName = request.getParameter("fname");
		 	String lastName = request.getParameter("lname");
		 	String street = request.getParameter("street");
		 	String city = request.getParameter("city");
		 	String state = request.getParameter("state");
		 	String zip = request.getParameter("zip");
		 	String phone = request.getParameter("phone");
		 	String email = request.getParameter("email");
		 	String balance = request.getParameter("balance");
		 	String totalSales = request.getParameter("total_sales");
		 	String notes = request.getParameter("notes");
		 	
		 //get update customer
		 Customer user = (Customer)session.getAttribute("user");
		 user.setId(cid);
		 user.setFname(firstName);
		 user.setLname(lastName); 
		 user.setStreet(street); 
		 user.setCity(city); 
		 user.setState(state); 
		 user.setZip(zip); 
		 user.setPhone(phone); 
		 user.setEmail(email); 
		 user.setBalance(balance); 
		 user.setTotalSales(totalSales); 
		 user.setNotes(notes);  
		 
		 url = "/modify.jsp";
		 //update customer
		 CustomerDB.update(user);
		 
		 ArrayList<Customer>users = CustomerDB.selectCustomers();
		 request.setAttribute("users", users);
		   }
		else if (action.equals("delete_customer"))
		  {
		//get customer
		Customer user = CustomerDB.selectCustomer(cid);
		
		url = "/modify.jsp";
		
		//delete customer
		CustomerDB.delete(user);
		 
		ArrayList<Customer>users = CustomerDB.selectCustomers();
		request.setAttribute("users", users);
		}
		
		getServletContext()
		 .getRequestDispatcher(url)
		 .forward(request, response);
		 
		}
		
		@Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		String url = "/modify.jsp";
		
		ArrayList<Customer> users= CustomerDB.selectCustomers();
		request.setAttribute("users", users);
		
		getServletContext()
		.getRequestDispatcher(url)
		.forward(request, response);
	}    
}
		 	
		  
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			