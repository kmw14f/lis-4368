> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Katherine Weachter

### Project # Requirements:

*Three parts:*

1. Create online portfolio
2. Edit JSP files
3. Create and modify regular expressions
4. Create and modify JS

#### README.md file should include the following items:

* Screenshot of failed data validation;
* Screenshot of passed data validation;


#### Project Screenshots:

*Screenshot of failed validation*:

![Main Page Screenshot](/p1/img/failed.png)

*Screenshot of passed validation*:

![Passed Screen shot](/p1/img/passed.png)



