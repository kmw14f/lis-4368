> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Katherine Weachter

### Assignment 4 # Requirements:


*3 Parts*

1. Perform client validation 
2. Analyze and debug failed client validation
3. Utilize JSP forms



#### Assignment 4 Screenshots:

*Screenshot of failed validation*:

![Query Results Screenshot](/a4/img/failed.png)


*Screenshot of Working Results*:

![Query Results Screenshot](/a4/img/working.png)

